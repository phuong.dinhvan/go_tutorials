package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	/// Slices are similar to arrays,but their size can be altered on runtime.
	/// Create an underlying array o 10 elements.
	mySlice := make([]int, 10)
	/// If we need to change the size of slice by:
	mySlice = append(mySlice, 5)


	// for _, var := range mySlice {
	// 	fmt.Printf("%d ", var)
	// }
	fmt.Printf("\n Total %d \n", len(mySlice))
	/// Delete the first item
	mySlice = mySlice[1:]
	fmt.Printf("\n Total %d \n", len(mySlice))
	
	mySlice = append(mySlice[1:], mySlice[2:]...)
	fmt.Printf("\n Total %d \n", len(mySlice))

	//mySliceRandom := make([]int, rand.Intn(100))
	// for index := 0; index < count; index++ {
		
	// }
}

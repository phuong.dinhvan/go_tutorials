package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	rand.Seed(time.Now().UnixNano())
	/// An array is a nmbered sequence of elements of a single type. You can store 100 different unsinged integers in a unique variable.
	/// Note: Their size cannot be changed.
	var arr1 [100]int
	for i := 0; i < 100; i++ {
		arr1[i] = rand.Intn(100)
	}
	for i := 0; i < 100; i++ {
		fmt.Printf("%d ", arr1[i])
	}
	fmt.Printf("\n");

	/// an array of size 3 with strings
	arr2 := [3]string{"go", "is", "awesome"}
	for i := 0; i < 3; i++ {
		fmt.Printf("%s; ", arr2[i])
	}
	fmt.Printf("\n");
	/// an array of 2 bool values that we initilize later:
	var arr3 [2]bool
	arr3[0] = true
	arr3[1] = false
	for i := 0; i < 2; i++ {
		fmt.Printf("%b; ", arr3[i])
	}
	fmt.Printf("\n");
}

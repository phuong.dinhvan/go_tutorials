package main

import (
	"fmt"
	"os"
	"path/filepath"
	"regexp"
)

func main() {
	var files []string

	root := "G:/temps/home"
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		files = append(files, path)
		return nil
	})
	if err != nil {
		panic(err)
	}
	fileRegexp := regexp.MustCompile(`[\w\-_]([/\\])data[(/|\\)]*[\w\-_]*(BCST_hose|BCST_hastc|BCST_upcom)`)
	for _, file := range files {
		// fmt.Println(file)
		if !fileRegexp.MatchString(file) {
			continue
		}
		fmt.Println("Matched: ", file)
	}
}

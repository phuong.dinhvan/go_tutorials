package main

import (
	"fmt"
	"os"
)

func fileStatus(filename string) (bool, error) {
	fmt.Printf("Checking file: %s\n", filename)
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		fmt.Println("File definitely does not exist.")
		return false, err
	}
	fmt.Printf("%s is existed", filename)
	return true, nil
}

func main() {
	_, err := fileStatus("D:/Dev/Golang/src/gitlab.com/phuong.dinhvan/go_tutorials/data/files/file01.txt")
	if err != nil {
		panic(err)
	}
	_, err = fileStatus("data/files/file01.txt")
	if err != nil {
		panic(err)
	}
}

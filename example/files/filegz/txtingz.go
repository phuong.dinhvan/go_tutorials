package filegz

import (
	"compress/gzip"
	"io/ioutil"
	"os"
)

/* ReadGzFile: Read text file in .GZ */
func ReadGzFile(filename string) ([]byte, error) {
	fi, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer fi.Close()

	fz, err := gzip.NewReader(fi)
	if err != nil {
		return nil, err
	}
	defer fz.Close()

	s, err := ioutil.ReadAll(fz)
	if err != nil {
		return nil, err
	}
	return s, nil
}

func main() {
	//gz file: D:/temps/20191105_170000.tar.gz
	//path: /home/v_igw/deployment/storages/20191105_170000/data/20191105_BCST_hose
	s, err := ReadGzFile("D:/temps/20191105_170000.tar.gz")
}

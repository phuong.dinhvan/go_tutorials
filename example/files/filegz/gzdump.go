package main

import (
	"archive/tar"
	"bufio"
	"compress/gzip"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"regexp"
)

// GZLines iterates over lines of a file that's gzip-compressed.
// Iterating lines of an io.Reader is one of those things that Go
// makes needlessly complex.
func GZLines(filename string) (chan []byte, chan error, error) {
	rawf, err := os.Open(filename)
	if err != nil {
		return nil, nil, err
	}
	rawContents, err := gzip.NewReader(rawf)
	if err != nil {
		return nil, nil, err
	}
	contents := bufio.NewScanner(rawContents)
	cbuffer := make([]byte, 0, bufio.MaxScanTokenSize)
	contents.Buffer(cbuffer, bufio.MaxScanTokenSize*50) // Otherwise long lines crash the scanner.
	ch := make(chan []byte)
	errs := make(chan error)
	go func(ch chan []byte, errs chan error, contents *bufio.Scanner) {
		defer func(ch chan []byte, errs chan error) {
			close(ch)
			close(errs)
		}(ch, errs)
		var (
			err error
		)
		for contents.Scan() {
			ch <- contents.Bytes()
		}
		if err = contents.Err(); err != nil {
			errs <- err
			return
		}
	}(ch, errs, contents)
	return ch, errs, nil
}

func ungzip(source, target string) error {
	reader, err := os.Open(source)
	if err != nil {
		return err
	}
	defer reader.Close()

	archive, err := gzip.NewReader(reader)
	if err != nil {
		return err
	}
	defer archive.Close()

	target = filepath.Join(target, archive.Name)
	writer, err := os.Create(target)
	if err != nil {
		return err
	}
	defer writer.Close()

	_, err = io.Copy(writer, archive)
	return err
}

func untar(tarball, target string) error {
	reader, err := os.Open(tarball)
	if err != nil {
		return err
	}
	defer reader.Close()
	tarReader := tar.NewReader(reader)

	for {
		header, err := tarReader.Next()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		path := filepath.Join(target, header.Name)

		info := header.FileInfo()
		if info.IsDir() {
			if err = os.MkdirAll(path, info.Mode()); err != nil {
				return err
			}
			continue
		}
		fmt.Println(header.Name)
		file, err := os.OpenFile(path, os.O_CREATE|os.O_TRUNC|os.O_WRONLY, info.Mode())
		if err != nil {
			return err
		}
		defer file.Close()
		_, err = io.Copy(file, tarReader)
		if err != nil {
			return err
		}
	}
	return nil
}
func main() {
	//fmt.Printf("Called on: %+v\n", os.Args)
	//gzfile := os.Args[1]
	gzfile := "G:/temps/20191105_170000.tar.gz"
	//_ = ungzip(gzfile, "D:/temps")
	//_ = untar("D:/temps/20191105_170000.tar", "D:/temps")

	_ = ExtractGzip(gzfile, "G:/temps")
	/*
		lines, errors, err := GZLines(gzfile)

		if err != nil {
			log.Fatal(err)
		}
		go func(errs chan error) {
			err := <-errs
			log.Fatal(err)
		}(errors)
		for foo := range lines {
			fmt.Printf("%+v\n", string(foo))
		}
	*/
}

func ExtractGzip(filename string, target string) error {
	rawf, err := os.Open(filename)
	if err != nil {
		return err
	}
	ExtractTarGz(rawf, target)
	return nil
}

func ExtractTarGz(gzipStream io.Reader, target string) {
	uncompressedStream, err := gzip.NewReader(gzipStream)
	if err != nil {
		log.Fatal("ExtractTarGz: NewReader failed")
	}

	tarReader := tar.NewReader(uncompressedStream)
	/// //path: /home/v_igw/deployment/storages/20191105_170000/data/20191105_BCST_hose
	/// 20191105_BCST_hose
	/// 20191105_BCST_hastc
	/// 20191105_BCST_upcom
	var fileRegexp = regexp.MustCompile(`([\w\-_](\/|\\)data(\/|\\)$|[\w\-_](\/|\\)data[(\/|\\)]*[\w\-_]*(BCST_hose|BCST_hastc|BCST_upcom))`)

	for true {
		header, err := tarReader.Next()

		if err == io.EOF {
			break
		}

		if err != nil {
			log.Fatalf("ExtractTarGz: Next() failed: %s", err.Error())
		}
		if !fileRegexp.MatchString(header.Name) {
			continue
		}
		fmt.Println("Matched ", header.Name)
		path := filepath.Join(target, header.Name)
		info := header.FileInfo()
		switch header.Typeflag {
		case tar.TypeDir:
			if err := os.MkdirAll(path, info.Mode()); err != nil {
				log.Fatalf("ExtractTarGz: Mkdir() failed: %s", err.Error())
			}
		case tar.TypeReg:
			outFile, err := os.Create(path)
			if err != nil {
				log.Fatalf("ExtractTarGz: Create() failed: %s", err.Error())
			}
			defer outFile.Close()
			if _, err := io.Copy(outFile, tarReader); err != nil {
				log.Fatalf("ExtractTarGz: Copy() failed: %s", err.Error())
			}
		default:
			log.Fatalf("ExtractTarGz: uknown type: %s in %s", header.Typeflag, header.Name)
		}
	}
}

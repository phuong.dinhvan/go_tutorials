package prsmsg

type PRSPackage struct {
	Header   PRSHeader
	Contents PRSBody
}

type PRSHeader struct {
	Msgnum    byte //the number of message in this package
	Type      byte //type of packge, currently it only has value 'N' (New)
	Seq       int
	ChannelID int
}

type PRSBody struct {
	Buffer []byte
}

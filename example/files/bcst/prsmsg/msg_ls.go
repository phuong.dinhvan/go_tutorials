package prsmsg

// PRS Message LS - Last Matched
type MsgLS struct {
	MsgType	string
	Symbol	string
	SecurityNumber	int32
	Side		string
	Price		int32
	Qty			int32
}


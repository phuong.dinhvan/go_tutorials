package main

import (
	"bufio"
	"errors"	
	"io"
	"log"
	"os"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"time"
	"encoding/hex"
)

/* Broadcast File Function */
type BCSTFileFunc func(string)

type BcstDataRow struct {
	UnixTime     int64
	FieldsNumber int
	BufferString string
	Buffer       []byte
}

type BcstPRSMessage struct {
	UnixTime int64
	Header   BcstPRSHeader
}

type BcstPRSHeader struct {
	Msgnum    byte //the number of message in this package
	Type      byte //type of packge, currently it only has value 'N' (New)
	Seq       int
	ChannelID int
}

func timeTrack(start time.Time, name string) {
	elapsed := time.Since(start)
	log.Printf("%s took %dms", name, elapsed.Nanoseconds()/1000)
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func limitLength(s string, length int) string {
	if len(s) < length {
		return s
	}

	return s[:length]
}

/* Convert String to Unix Time */
func Str2unix_bcst(strTime string) (int64, error) {
	t, e := time.Parse("20060102 15:04:05", strTime)
	if e != nil {
		return 0, e
	}
	return t.Unix(), nil
}

/* Convert Buffer String to Buffer */
func Str2Buffer_bcst(dumpData BcstDataRow) (*BcstDataRow, error) {
	listBytes := strings.Fields(dumpData.BufferString)
	if len(listBytes) != dumpData.FieldsNumber {
		return nil, errors.New("Buffer String is invalid")
	}
	for index := 0; index < dumpData.FieldsNumber; index++ {
		data, err := hex.DecodeString(listBytes[index])
		if err != nil {
			continue
		}
		dumpData.Buffer[index] = data[0]
	}
	//fmt.Println(len(listBytes)) // [one two three four] 4
	return &dumpData, nil
}

func main() {
	path := "G:/temps/home"
	_ = travelFiles(path, bcstDump)
}

func travelFiles(path string, fp BCSTFileFunc) error {
	var files []string
	err := filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
		files = append(files, path)
		return nil
	})
	if err != nil {
		panic(err)
	}
	var fileRegexp = regexp.MustCompile(`[\w\-_](\/|\\)data[(\/|\\)]*[\w\-_]*(BCST_hose|BCST_hastc|BCST_upcom)`)
	for _, file := range files {
		//fmt.Println(file)
		if !fileRegexp.MatchString(file) {
			continue
		}
		//fmt.Println("Matched: ", file)
		if fp != nil {
			start := time.Now()
			fp(file)
			elapsed := time.Since(start)
			log.Printf("Parsing %s", elapsed)
		}
	}
	return nil
}

func bcstDump(filename string) {
	defer timeTrack(time.Now(), "bcstDump")
	log.Println("Dump: ", filename)
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	re := regexp.MustCompile("\\[(.*?)\\]")
	//re := regexp.MustCompile("(\\[(.*?)\\]|(.*?))")
	// Start reading from the file with a reader.
	reader := bufio.NewReader(file)
	var line string
	var lineNumber int
	for {
		line, err = reader.ReadString('\n')

		if err != nil {
			break
		}
		lineNumber++
		if len(line) < 40 {
			continue
		}
		match := re.FindAllStringSubmatch(line, -1)
		elements := re.Split(line, 7)
		// for _, element := range elements {
		// 	fmt.Println(element)
		// }

		unixTime, e1 := Str2unix_bcst(match[0][1])
		if e1 != nil {
			//panic(e)
			continue
		}
		buflen, e2 := strconv.Atoi(match[5][1])
		if e2 != nil {
			//panic(e)
			continue
		}
		var dumpData = BcstDataRow{unixTime, buflen, elements[6], make([]byte, buflen)}
		_, e3 := Str2Buffer_bcst(dumpData)
		if e3 != nil {
			continue
		}

		//fmt.Printf("%s -> %d\n", match[0][1], unixTime)

	}
	if err != io.EOF {
		log.Printf(" > Failed!: %v\n", err)
	}
	log.Printf("Line number: %d\n", lineNumber)
	return
}

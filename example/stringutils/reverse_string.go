package stringutils

/*
	Visibility is the attribute of a function or a variable to visible to different parts of the program.
	+) Uppercase definitions are public (visible in the entire program)
	+) Lowercase are private (not seen at the package level)
*/

func Reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

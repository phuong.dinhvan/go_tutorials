package main

import (
	"fmt"
	"gitlab.com/phuong.dinhvan/go_tutorials/example/stringutils"
)

func main() {
	fmt.Println("Hello World!")
	rstr := stringutils.Reverse("test reverse")
	fmt.Println(rstr)
}

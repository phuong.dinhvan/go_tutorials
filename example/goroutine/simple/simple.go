package main

import (
 "fmt"
 "time"
)
func f(from string) {
	for index := 0; index < 10; index++ {
		fmt.Println(from, ":", index)
	}
}

/*
	A goroutine is a lightweight thread of execution.
	Our two function calls are running asynchronously in separate goroutines now. Wait for them to finish (for a more robust approach, use a WaitGroup).
*/
func main()  {
	go f("goroutine")
	f("direct")

	go func(msg string) { fmt.Println(msg)}("Going")
	time.Sleep(time.Second)
	fmt.Println("done")
}
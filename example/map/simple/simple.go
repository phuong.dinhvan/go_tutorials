package main

import "fmt"

import "encoding/json"

func main() {

	//// Simple Map
	myMap := make(map[string]int)
	myMap["one"] = 1
	myMap["two"] = 2
	fmt.Println(myMap["one"])

	//// Json Map
	myJsonMap := make(map[string]interface{})
	jsonData := []byte(`{"hello": "world"}`)
	err := json.Unmarshal(jsonData, &myJsonMap)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s\n", myJsonMap["hello"])
}

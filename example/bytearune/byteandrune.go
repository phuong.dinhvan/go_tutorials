package main

import (
	"bytes"
	"fmt"
)

/* Byte and Rune Data Type
		Golang has integer types called byte and rune that are aliases for uint8 and int32 data types, respectively.
		In Go, there is no char data type. It uses byte and rune to represent character values.
		The byte data type represents ASCII characters while the rune data type represents a more broader set of Unicode characters that are encoded in UTF-8 format.
		In Go, Characters are expressed by enclosing them in single quotes like this: 'a'.
		The default type for character values is rune, which means, if we don't declare a type explicitly when declaring a variable with a character value, then Go will infer the type as rune.
*/

func main() {
	// Note _ : implicit declaration (not show error)
	//var myLetter = 'R'           // Type inferred as rune which is the default type for character values
	//var anotherLetter byte = 'B' //ASCII
	myByte := byte('b')
	myRune := 'b'
	fmt.Println("%c=%d   %c= %U", myByte, myByte, myRune, myRune)

	b1 := byte('a')
	b2 := []byte("A")
	b3 := []byte{'a', 'b', 'c'}
	fmt.Printf("b1 = %c\n", b1)
	fmt.Printf("b2 = %c\n", b2)
	fmt.Printf("b3 = %s\n", b3)
	s1 := []byte("Hello")
	s2 := []byte("World")
	s3 := [][]byte{s1, s2}
	s4 := bytes.Join(s3, []byte(","))
	s5 := []byte{}
	s5 = bytes.Join(s3, []byte("--"))
	s6 := [][]byte{[]byte("foo"), []byte("bar"), []byte("baz")}
	fmt.Printf("s1 = %s\n", s1)
	fmt.Printf("s2 = %s\n", s2)
	fmt.Printf("s3 = %s\n", s3)
	fmt.Printf("s4 = %s\n", s4)
	fmt.Printf("s5 = %s\n", s5)
	fmt.Printf("%s\n", bytes.Join(s6, []byte(", ")))
}

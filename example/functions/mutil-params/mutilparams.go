package main

import "fmt"

func sum(args ...int) (result int) {
	for _, v := range args {
		result += v
	}
	return result
}

func main()  {
	fmt.Printf("Sum(1,2,3) = %d\n", sum(1,2,3))
	fmt.Printf("Sum(4,5,6,7,8) = %d\n", sum(4,5,6,7,8))
	fmt.Printf("Sum() = %d\n", sum())
}


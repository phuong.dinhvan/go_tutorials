package main

import "fmt"

func hello(msg string) {
	fmt.Printf("hello, msg= %s\n", msg)
}

func main() {
	//regular function
	hello("regular function")

	func(msg string) {
		fmt.Printf("Hello, msg= %s", msg)
	}("anonymous function")
}

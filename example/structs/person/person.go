package main

import (
	"fmt"
	"strconv"
)

// Struct Person
type Person struct {
	firstname string
	lastname  string
	city      string
	gender    string
	age       int
}

//method (value eceiver)
func (p Person) hello() string {
	return "Hello, I am " + p.firstname + " " + p.lastname + ", " + strconv.Itoa(p.age) + " years old"
}

//method (pointer receiver)
func (p *Person) hasBirthday() {
	p.age++
}

func main() {
	p1 := Person{firstname: "David", lastname: "Jorh", city: "Hanoi", gender: "Male", age: 35}
	p2 := Person{firstname: "Celin", lastname: "Joe", city: "Hanoi", gender: "Male", age: 33}
	fmt.Println(p1.hello())
	fmt.Println(p2.hello())

	p2.hasBirthday()
	fmt.Println(p2.hello())
}

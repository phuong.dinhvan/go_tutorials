package main

import "fmt"
import "errors"

func main() {

	// create error
	myErr := errors.New("Something unexpected happend!")

	// print error type
	fmt.Printf("Type of myErr is %T \n", myErr)
	fmt.Printf("Value of myErr is %#v \n", myErr)
}

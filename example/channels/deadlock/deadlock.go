package main

import "fmt"

/*
	One important factor to consider while using channels is deadlock. If a Goroutine is sending data on a channel,
	then it is expected that some other Goroutine should be receiving the data.
	If this does not happen, then the program will panic at runtime with Deadlock.
*/
func main() {
	/*
		In the program above, a channel ch is created and we send 5 to the channel in line ch <- 5. In this program no other Goroutine is receiving data from the channel ch.
		Hence this program will panic with the following runtime error.
	*/
	ch := make(chan int)
	fmt.Println("Normal")
	go writeToChannel(ch)
	//readChannel(ch)
	fmt.Printf("value: %d \n", <-ch)

	fmt.Println("Deadlock !!!!!")
	ch <- 5
}

func writeToChannel(ch chan int) {
	ch <- 5
}

func readChannel(ch chan int) {
	read := <-ch
	fmt.Printf("value: %d \n", read)
}

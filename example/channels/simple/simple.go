package main

import (
	"fmt"
	"time"
)

/*
	What are channels
	Channels can be thought as pipes using which Goroutines communicate. Similar to how water flows from one end to another in a pipe, data can be sent from one end
	and received from the another end using channels
*/
func main() {
	//Declare a channel
	var a chan int
	if a == nil {
		fmt.Println("channel a is nil, going to define it")
		a = make(chan int)
		fmt.Printf("Type of a is %T\n", a)
	}

	/// Read & Write Channel
	done := make(chan bool)
	fmt.Println("Main going to call hello go goroutine")
	go hello(done)
	_ = <-done
	fmt.Println("Main received data")
}

func hello(done chan bool) {
	fmt.Println("		hello go routine is going to sleep")
	time.Sleep(4 * time.Second)
	fmt.Println("		hello go routine awake and going to write to done")
	done <- true
}

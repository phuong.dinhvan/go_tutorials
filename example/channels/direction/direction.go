package main

import (
	"fmt"
	"time"
)

func ping(pings chan<- string, msg string) {
	pings <- msg
}
func pong(pings chan string, pongs chan<- string) {
	msg := <-pings
	pongs <- msg
}

func main() {
	pings := make(chan string, 1)
	pongs := make(chan string, 1)
	for index := 0; index < 10; index++ {
		msg:= fmt.Sprintf("Message %d", index)
		fmt.Println("PING ", msg)
		ping(pings, msg)
		pong(pings, pongs)
		recv:= <- pongs
		fmt.Println("PONG ", recv)
		time.Sleep(1*time.Second)
	}
	fmt.Println("Finished")
}

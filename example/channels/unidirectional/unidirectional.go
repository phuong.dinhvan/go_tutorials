package main

import "fmt"

func sendData(sendch chan<- int) {
	sendch <- 10
}

//channels that only send or receive data
func main() {

	//make send-only channel
	sendch := make(chan<- int)
	go sendData(sendch)
	read := <-sendch
	fmt.Println(read)
}

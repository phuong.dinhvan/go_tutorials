package builder

// BusBuilder --
type BusBuilder struct {
	vehicle 	VehicleProduct
}

// SetWheels --
func (b *BusBuilder) SetWheels() BuildProcess {
	b.vehicle.Wheels = 4 * 2
	return b
}

// SetSeas --
func (b *BusBuilder) SetSeats() BuildProcess {
	b.vehicle.Seats = 30
	return b
}

// SetStructure --
func (b *BusBuilder) SetStructure() BuildProcess {
	b.vehicle.Structure = "Bus"
	return b
}

func (b* BusBuilder) GetVehicle() VehicleProduct {
	return b.vehicle
}

func (b *BusBuilder) Build() VehicleProduct {
	return VehicleProduct{}
}

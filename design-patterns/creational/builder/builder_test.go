package builder

import (
	"testing"
)

/*
	Command Lines:
	cd $PRJ_DIR/go_tutorials/design-patterns/creational/builder
	go test -v -run=TestBuilder .
*/

func TestCarBuilderPattern(t *testing.T) {
	manufactoringComplex := ManufacturingDirector{}
	carBuilder := &CarBuilder{}

	manufactoringComplex.SetBuilder(carBuilder)
	manufactoringComplex.Construct()

	car := carBuilder.GetVehicle()
	if car.Wheels != 4 {
		t.Errorf("Wheels on a car must be 4 and they were %d\n", car.Wheels)
	}

	if car.Seats < int(4) {
		t.Errorf("Seats on a car must greater than 4 and they were %d\n", car.Seats)
	}
	if car.Structure != "Car" {
		t.Errorf("Structure on a car must be 'Car' and they were %s\n", car.Structure)
	}

}

func TestBikeBuilderPattern(t *testing.T) {
	manufactoringComplex := ManufacturingDirector{}
	bikeBuilder := &BikeBuilder{}

	manufactoringComplex.SetBuilder(bikeBuilder)
	manufactoringComplex.Construct()

	bike := bikeBuilder.GetVehicle()
	if bike.Wheels != 2 {
		t.Errorf("Wheels on a car must be 2 and they were %d\n", bike.Wheels)
	}

	if bike.Seats != 2 {
		t.Errorf("Seats on a car must be 2 and they were %d\n", bike.Seats)
	}
	if bike.Structure != "MotorBike" {
		t.Errorf("Structure on a car must be 'Bike' and they were %s\n", bike.Structure)
	}

}

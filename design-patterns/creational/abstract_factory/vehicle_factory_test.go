package abstractfactory

import "testing"

// VehicleFactory --
func TestMotorbikeFactory(t *testing.T) {
	factory, err := BuildFactory(MotorbikeFactoryType)
	if err != nil {
		t.Fatal(err)
	}
	// Make Sport motorbike
	motorbikeVehicle, err := factory.Build(SportMotorbikeType)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("Motorbike vehicle has %d wheels, %d seats \n", motorbikeVehicle.NumWheels(), motorbikeVehicle.NumSeats())
	// Asserting a vehicle instance is a Motorbike Type
	sportbike, ok := motorbikeVehicle.(Motorbike)
	if !ok {
		t.Fatal("Struct assertion has failed")
	}
	t.Logf("Sport motorbike has type %d \n", sportbike.GetMotorbikeType())

	// Make cruise motorbike
	motorbikeVehicle, err = factory.Build(CruiseMotorbikeType)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("Motorbike vehicle has %d wheels, %d seats \n", motorbikeVehicle.NumWheels(), motorbikeVehicle.NumSeats())
	// Asserting a vehicle instance is a Motorbike Type
	cruisebike, ok := motorbikeVehicle.(Motorbike)
	if !ok {
		t.Fatal("Struct assertion has failed")
	}
	t.Logf("Cruise motorbike has type %d \n", cruisebike.GetMotorbikeType())

	//Make unknown motorbike type
	motorbikeVehicle, err = factory.Build(5)
	if err == nil {
		t.Fatal(err)
	}
	t.Logf(err.Error())

}

func TestCarFactory(t *testing.T) {
	factory, err := BuildFactory(CarFactoryType)
	if err != nil {
		t.Fatal(err)
	}
	carVehicle, err := factory.Build(LuxuryCarType)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("Car vehicle has %d wheels, %d seats \n", carVehicle.NumWheels(), carVehicle.NumSeats())
	// Asserting a vehicle is a Car Type
	luxCar, ok := carVehicle.(Car)
	if !ok {
		t.Fatal("Struct assertion has failed")
	}
	t.Logf("Luxury car has %d doors \n", luxCar.NumDoors())

	///
	//
	///
	carVehicle, err = factory.Build(FamilyCarType)
	if err != nil {
		t.Fatal(err)
	}
	t.Logf("Car vehicle has %d wheels, %d seats \n", carVehicle.NumWheels(), carVehicle.NumSeats())
	// Asserting a vehicle is a Car Type
	falmilyCar, ok := carVehicle.(Car)
	if !ok {
		t.Fatal("Struct assertion has failed")
	}
	t.Logf("Luxury car has %d doors \n", falmilyCar.NumDoors())

	//Make unknown car type
	carVehicle, err = factory.Build(5)
	if err == nil {
		t.Fatal(err)
	}
	t.Logf(err.Error())
}

package abstractfactory

import (
	"errors"
	"fmt"
)

const (
	// LuxuryCarType --
	LuxuryCarType = 1
	// FamilyCarType --
	FamilyCarType = 2
)

// Car --
type Car interface {
	NumDoors() int
}

// CarFactory --
type CarFactory struct{}

// NewVehicle --
func (c *CarFactory) Build(v int) (Vehicle, error) {
	switch v {
	case LuxuryCarType:
		return new(LuxuryCar), nil
	case FamilyCarType:
		return new(FamilyCar), nil
	default:
		return nil, errors.New(fmt.Sprintf("Vehicle of type %d not recognized", v))
	}
}

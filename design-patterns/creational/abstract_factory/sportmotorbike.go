package abstractfactory

type SportMotorbike struct {}


// NumWheels --
func (*SportMotorbike) NumWheels() int {
	return 2
}

// NumSeats --
func (*SportMotorbike) NumSeats() int {
	return 2
}

func (s *SportMotorbike) GetMotorbikeType() int {
	return SportMotorbikeType
}
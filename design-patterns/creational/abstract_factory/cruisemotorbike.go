package abstractfactory

type CruiseMotorbike struct {}


// NumWheels --
func (*CruiseMotorbike) NumWheels() int {
	return 2
}

// NumSeats --
func (*CruiseMotorbike) NumSeats() int {
	return 2
}

func (s *CruiseMotorbike) GetMotorbikeType() int {
	return CruiseMotorbikeType
}
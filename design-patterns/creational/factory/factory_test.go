package factory

import (
	"strings"
	"testing"
)

func TestCreatePaymentMethodCash(t *testing.T) {
	payment, err := GetPaymentMethod(Cash)
	if err != nil {
		t.Fatal("A Payment method of type 'Cash' must exist")
	}
	msg := payment.Pay(10.30)

	if !strings.Contains(msg, "paid using cash") {
		t.Error("The cash payment method message wasn't correct")
	}
	t.Log("LOG: ", msg)
}

func TestCreatePaymentMethodDebitCard(t *testing.T) {
	payment, err := GetPaymentMethod(DebitCard)
	if err != nil {
		t.Fatal("A Payment method of type 'DebitCard' must exist")
	}
	msg := payment.Pay(22.30)

	if !strings.Contains(msg, "paid using debit card") {
		t.Error("The cash payment method message wasn't correct")
	}
	t.Log("LOG: ", msg)
}

func TestCreatePaymentMethodNotExistent(t *testing.T) {
	method := 5
	_, err := GetPaymentMethod(method)
	if err == nil {
		t.Errorf("A Payment method with ID %d must return an error", method)
	}
	t.Log("LOG: ", err)
}
package prototype

import "testing"

func TestClone(t *testing.T) {
	shirtCache := GetShirtCloner()
	if shirtCache == nil {
		t.Fatal("Received cache was nil")
	}
	item1, err := shirtCache.GetClone(Blue)

	if err != nil {
		t.Fatal(err)
	}
	if item1 == whitePrototype {
		t.Error("item1 cannot be equal to the white prototype")
	}
	shirt1, ok := item1.(*Shirt)
	if !ok {
		t.Fatal("Type asserttion for shirt1 couldn't be done successfully")
	}
	shirt1.SKU = "abbcc"
	item2, err := shirtCache.GetClone(Black)
	if err != nil {
		t.Fatal(err)
	}
	shirt2, ok := item2.(*Shirt)
	if !ok {
		t.Fatal("Type asserttion for shirt1 couldn't be done successfully")
	}
	if shirt1.SKU == shirt2.SKU {
		t.Error("SKU's of shirt1 and shirt2 must be different")
	}

	if shirt1 == shirt2 {
		t.Error("Shirt 1 can not equal to Shirt 2")
	}
	t.Logf("LOG: %s \n", shirt1.GetInfo())
	t.Logf("LOG: %s \n", shirt2.GetInfo())
	t.Logf("LOG: The memory possitions of the shirts are different %p != %p \n\n", &shirt1, &shirt2)
}

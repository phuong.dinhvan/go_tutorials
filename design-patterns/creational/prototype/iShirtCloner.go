package prototype

type ShirtCloner interface {
	GetClone(s int) (ItemInfoGetter, error)
}

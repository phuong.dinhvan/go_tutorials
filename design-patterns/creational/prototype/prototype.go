package prototype

type ItemInfoGetter interface {
	GetInfo() string
}

func GetShirtCloner() ShirtCloner {
	return new(ShirtCache)
}

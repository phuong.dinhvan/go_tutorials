package prototype

import "fmt"

const (
	White = 1
	Black = 2
	Blue  = 3
)

type ShirtColor byte
type Shirt struct {
	Price float32
	SKU   string
	Color ShirtColor
}

func (s *Shirt) GetInfo() string {
	return fmt.Sprintf("Shirt with SKU '%s' and Color id %d that costs %f \n", s.SKU, s.Color, s.Price)
}
